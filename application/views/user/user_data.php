<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Users</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Users</h3>
            <div class="pull-right">
                <a href="<?=site_url('user/add')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-user-plus"></i>
                    Create
                </a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Level</th>
                        <th>Action</th>
                    
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        foreach ($row->result() as $key => $data) {
        
                    ?>
                    <tr>
                        <td><?= $no++?></td>
                        <td><?= $data->username?></td>
                        <td><?= $data->password?></td>
                        <td><?= $data->name?></td>
                        <td><?= $data->address?></td>
                        <td><?= $data->level == 1 ? "ADMIN":"KASIR" ?></td>
                        <td>
                            <form action="<?=site_url('user/del')?>" method="post">
                                <!-- edit -->
                                <a href="<?=site_url('user/edit/'.$data->user_id)?>" class="btn btn-warning btn-xs">
                                    <i class="fa fa-pencil"></i>
                                    Edit
                                </a>
                                <!-- delete -->
                                <input type="hidden" name="user_id" value="<?=$data->user_id?>">
                                <button type="submit" onclick="return confirm('apakah anda yakin?')" class="btn btn-danger btn-xs">
                                    <i class="fa fa-trash"></i>Delete
                                </button>
                            </form>
                            

                        </td>
                    </tr>
                    <?php 
                        }
                    ?>

                </tbody>
            </table>
        </div>

    </div>


</section>