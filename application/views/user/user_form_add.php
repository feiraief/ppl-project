<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Users</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add Data Users</h3>
            <div class="pull-right">
                <a href="<?=site_url('user')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-undo"></i>
                    Back
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">

                    <form action="" method="post">
                        <div class="form-group <?=form_error('fullname') ? 'has-error' : null ?>">
                            <label for="">Name*</label>
                            <input type="text" name="fullname" value="<?=set_value('fullname')?>" class="form-control">
                            <?=form_error('fullname')?>
                        </div>
                        <div class="form-group <?=form_error('username') ? 'has-error' : null ?>">
                            <label for="">Username*</label>
                            <input type="text" name="username" value="<?=set_value('username')?>" class="form-control">
                            <small><?=form_error('username')?></small>
                        </div>
                        <div class="form-group <?=form_error('password') ? 'has-error' : null ?>">
                            <label for="">Password*</label>
                            <input type="password" name="password" class="form-control">
                            <small><?=form_error('password')?></small>
                        </div>
                        <div class="form-group <?=form_error('passconf') ? 'has-error' : null ?>">
                            <label for="">Password Confirmation*</label>
                            <input type="password" name="passconf" class="form-control">
                            <small><?=form_error('passconf')?></small>
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <textarea name="address" class="form-control"><?=set_value('address')?></textarea>
                        </div>
                        <div class="form-group <?=form_error('level') ? 'has-error' : null ?>">
                            <label for="">Level*</label>
                            <select name="level" class="form-control">
                                <option value="">== PILIH ==</option>
                                <option value="1" <?=set_value('level') == 1 ? "selected" : null?>>Admin</option>
                                <option value="2" <?=set_value('level') == 2 ? "selected" : null?>>Kasir</option>
                            </select>
                            <small><?=form_error('level')?></small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-flat">Save</button>
                            <button type="reset" class="btn btn-flat">Reset</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>


</section>