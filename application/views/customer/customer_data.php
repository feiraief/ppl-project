<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        customers
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">customer</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data customers</h3>
            <div class="pull-right">
                <a href="<?=site_url('customer/add')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-user-plus"></i>
                    Create
                </a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Phone</th>
                        <th>Address</th>
                        <th>Action</th>
                    
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $no = 1;
                        foreach ($row->result() as $key => $data) {
        
                    ?>
                    <tr>
                        <td><?= $no++?></td>
                        <td><?= $data->name?></td>
                        <td><?= $data->gender?></td>
                        <td><?= $data->phone?></td>
                        <td><?= $data->address?></td>
                        <td>
                            
                            <a href="<?=site_url('customer/edit/'.$data->customer_id)?>"  class="btn btn-warning btn-xs">
                                <i class="fa fa-pencil"></i>
                                edit
                            </a>
                            <!-- delete -->
                            <a href="<?=site_url('customer/del/'.$data->customer_id)?>" onclick="return confirm('apakah anda yakin?')" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash"></i>
                                Delete
                            </a>
               
                            
                        </td>
                    </tr>
                    <?php 
                        }
                    ?>

                </tbody>
            </table>
        </div>

    </div>


</section>