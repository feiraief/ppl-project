<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        items
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">item</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?php $this->view('message')?>
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data items</h3>
            <div class="pull-right">
                <a href="<?=site_url('item/add')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-user-plus"></i>
                    Create
                </a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Barcode</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Unit</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Image</th>
                        <th>Action</th>
                    
                    </tr>
                </thead>
                <tbody>
                    <!-- <?php
                        $no = 1;
                        foreach ($row->result() as $key => $data) {
        
                    ?>
                    <tr>
                        <td><?= $no++?></td>
                        <td><?= $data->barcode?></td>
                        <td>
                            <?php if($data->image != null){?>
                            <img src="<?=base_url('uploads/product/'.$data->image)?>" width=100>
                            <?php }?>
                        </td>
                        <td><?= $data->name?></td>
                        <td><?= $data->category_name?></td>
                        <td><?= $data->unit_name?></td>
                        <td><?= $data->price?></td>
                        <td><?= $data->stock?></td>
                        <td>
                            
                            <a href="<?=site_url('item/edit/'.$data->item_id)?>"  class="btn btn-warning btn-xs">
                                <i class="fa fa-pencil"></i>
                                edit
                            </a>
                            
                            <a href="<?=site_url('item/del/'.$data->item_id)?>" onclick="return confirm('apakah anda yakin?')" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash"></i>
                                Delete
                            </a>
               
                            
                        </td>
                    </tr>
                    <?php 
                        }
                    ?> -->

                </tbody>
            </table>
        </div>

    </div>


</section>
<script>
  $(document).ready(function () {
    $('#table1').DataTable({
        "processing" : true,
        "serverSide" : true,
        "ajax" : {
           "url" : "<?=site_url('item/get_ajax')?>",
           "type" : "POST"

        },
        "columnDefs":[
            {
            "targets" : [7,8],
            "className" : 'text-center'
            },
            {
            "targets" : [0,7,8],
            "orderable" : false
            }
        ],
        "order":[]
    })
  })

</script>