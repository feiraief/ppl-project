<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        items
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">item</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
<?php $this->view('message')?>
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"><?=ucfirst($page)?> item</h3>
            <div class="pull-right">
                <a href="<?=site_url('item')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-undo"></i>
                    Back
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">

                    <?php echo form_open_multipart('item/process')?>
                    
                        <div class="form-group">
                            <label for="">barcode*</label>
                            <input type="hidden" name="id" value="<?=$row->item_id?>">
                            <input type="text" name="barcode" value="<?=$row->barcode?>" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">product Name*</label>
                            <input type="text" name="item_name" value="<?=$row->name?>" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">category*</label>
                            <select name="category" class="form-control" required>
                                <option value="">--PILIH--</option>
                                <?php foreach ($category->result() as $key => $data) { ?>
                                    <option value="<?=$data->category_id?>" <?= $data->category_id == $row->category_id ? "selected" : null ?> ><?=$data->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">unit*</label>
                            <select name="unit" class="form-control" required>
                                <option value="">--PILIH--</option>
                                <?php foreach ($unit->result() as $key => $data) { ?>
                                    <option value="<?=$data->unit_id?>"<?= $data->unit_id == $row->unit_id ? "selected" : null ?>><?=$data->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">price*</label>
                            <input type="number" name="price" value="<?=$row->price?>" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="">image</label>
                            <?php if($page == 'edit'){
                                if ($row->image != null) { ?>
                                    <div>
                                        <img src="<?=base_url('uploads/product/'.$row->image)?>" width=50%>
                                    </div>
                                <?php
                                }
                            }?>
                            <input type="file" name="image" value="<?=$row->price?>" class="form-control">
                        </div>

                        
                        <div class="form-group">
                            <button type="submit" name="<?=$page?>" class="btn btn-success btn-flat">Save</button>
                            <button type="reset" class="btn btn-flat">Reset</button>
                        </div>
                    <?php echo form_close() ?>

                </div>
            </div>
        </div>

    </div>


</section>