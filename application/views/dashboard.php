<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- dashboard page -->
    <?php 
        foreach ($habis->result() as $key => $data){
        
    ?>
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Perhatian</strong> Stock barang <b><?=$data->name?></b> Sudah habis
    </div>
    <?php 
        }
    ?>
    <?php 
        foreach ($row->result() as $key => $data){
        
    ?>
    <div class="alert alert-warning alert-dismissible">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Perhatian</strong> Stock barang <b><?=$data->name?></b> Hampir habis
    </div>
    <?php 
        }
    ?>

    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-th"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Items</span>
                    <span class="info-box-number"><?= $this->fungsi->count_item()?></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-grey"><i class="fa fa-truck"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Supplier</span>
                    <span class="info-box-number"><?= $this->fungsi->count_supplier()?></span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">users</span>
                    <span class="info-box-number"><?= $this->fungsi->count_user()?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="text-center" class="box-title">HAMPIR KOSONG</h3>
                    
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>Stock</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <?php $no = 1; foreach ($row->result() as $key => $data) {?>
                            <td><?=$no++?></td>
                            <td><?=$data->name?></td>
                            <td><?=$data->stock?></td>
                            <tr>
                            <?php 
                            } 
                            ?>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="text-center" class="box-title">BARANG KOSONG</h3>
                    
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Barang</th>
                                <th>Stock</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                            <?php $no = 1; foreach ($habis->result() as $key => $data) {?>
                            <td><?=$no++?></td>
                            <td><font color="red"><?=$data->name?></td></font>
                            <td><font color="red"><?=$data->stock?></td></font>
                            <tr>
                            <?php 
                            } 
                            ?>
    
                        </tbody>
                    </table>
                </div>
    
            </div>
        </div>
        
    </div>
    
        

    
    
</section>