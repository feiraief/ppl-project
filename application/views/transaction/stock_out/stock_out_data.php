<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
       Stock OUT
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">stockout</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <?php $this->view('message')?>
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Data Stock OUT</h3>
            <div class="pull-right">
                <a href="<?=site_url('stock/out/add')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-user-plus"></i>
                    Create
                </a>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="table1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Barcode</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th class="text-center">Unit</th>
                        <th class="text-center">Stock OUT</th>
                        <th>Date</th>
                        <th class="text-center">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    $no = 1;
                    foreach ($row as $key => $data) { ?>
                       <tr>
                           <td><?=$no++?></td>
                           <td><?=$data->barcode?></td>
                           <td><?=$data->item_name?></td>
                           <td><?=$data->category_name?></td>
                           <td class="text-center"><?=$data->unit_name?></td>
                           <td class="text-center"><?=$data->qty?></td>
                           <td><?=indo_date($data->date)?></td>
                           <td class="text-center">
                            
                            <a id ="set_detail" class="btn btn-default btn-xs" 
                                data-toggle="modal" data-target="#modal-detail"
                                data-barcode = "<?=$data->barcode?>"
                                data-item = "<?=$data->item_name?>"
                                data-detail = "<?=$data->detail?>"
                                data-qty = "<?=$data->qty?>"
                                data-date = "<?=$data->tanggal?>"
                            >
                                
                                <i class="fa fa-eye"></i>
                                Detail
                            </a>
                            <!-- delete -->
                            <a href="<?=site_url('stock/out/del/'.$data->stock_id.'/'.$data->item_id)?>" onclick="return confirm('apakah anda yakin?')" class="btn btn-danger btn-xs">
                                <i class="fa fa-trash"></i>
                                Delete
                            </a>
                           </td>
                       </tr>
                    <?php }?>
                    

                </tbody>
            </table>
        </div>

    </div>


</section>

<div class="modal fade" id="modal-detail">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dissmiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detail Stock OUT</h4>
            </div>
            <div class="modal-body table-responsive">
                <table class="table table-bordered table-striped">
                    <tbody>
                        <tr>
                            <th>Barcode</th>
                            <td><span id="barcode"></span></td>
                        </tr>
                        <tr>
                            <th>Name</th>
                            <td><span id="item_name"></span></td>
                        </tr>
                        
                        <tr>
                            <th>Detail</th>
                            <td id="detail"></td>
                        </tr>
                        <tr>
                            <th>QTY</th>
                            <td><span id="qty"></span></td>
                        </tr>
                        <tr>
                            <th>Date & Time</th>
                            <td><span id="date"></span></td>
                        </tr>
                        </tbody>
                </table>
                
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','#set_detail',function () {
            var barcode = $(this).data('barcode');
            var item_name = $(this).data('item');
            var detail = $(this).data('detail');
            
            var qty = $(this).data('qty');
            var date = $(this).data('date');

            $('#barcode').text(barcode);
            $('#item_name').text(item_name);
            $('#detail').text(detail);
            
            $('#qty').text(qty);
            $('#date').text(date);
            
        })
        
    })
</script>