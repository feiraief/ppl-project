<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Stock IN
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i></a></li>
        <li class="active">stockin</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add Stock IN</h3>
            <div class="pull-right">
                <a href="<?=site_url('stock/in')?>" class="btn btn-primary btn-flat">
                    <i class="fa fa-undo"></i>
                    Back
                </a>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">

                    <form action="<?=site_url('stock/process')?>" method="post">
                        <div class="form-group">
                            <label for="">Date*</label>
                            <input type="date" name="date" value="<?=date('Y-m-d')?>" class="form-control" required>
                        </div>

                        <div>
                            <label for="barcode">Barcode*</label>
                        </div>
                        <div class="form-group input-group">
                            <input type="hidden" name="item_id" id="item_id">
                            <input type="text" name="barcode" id="barcode" class="form-control" required autofocus>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-info btn-flat" data-toggle="modal" data-target="#modal-item">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>

                        <div class="form-group">
                            <label for="item_name">Item Name</label>
                            <input type="text" name="item_name" id="item_name" value="" class="form-control" readonly>
                        </div>
                        <div class="row">
                            
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label for="category_name">Category Name</label>
                                    <input type="text" name="category_name" id="category_name" value="" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="unit_name">Unit</label>
                                    <input type="text" name="unit_name" id="unit_name" value="-" class="form-control" readonly>
                                </div>
                                
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="stock">Stock</label>
                                    <input type="text" name="stock" id="stock" value="" class="form-control" readonly>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="detail">Detail</label>
                            <input type="text" name="detail" id="detail" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="supplier">Supplier</label>
                            <select name="supplier" class="form-control">
                                <option value="">--PILIH--</option>
                                <?php foreach($supplier as $key => $data){
                                    echo '<option value="'.$data->supplier_id.'">'.$data->name.'</option>';
                                } ?>
                                
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="qty">QTY*</label>
                            <input type="text" name="qty" id="qty" value="" class="form-control" required>
                        </div>

                        
                        <div class="form-group">
                            <button type="submit" name="in_add" class="btn btn-success btn-flat">Save</button>
                            <button type="reset" class="btn btn-flat">Reset</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>

    </div>


</section>

<div class="modal fade" id="modal-item">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dissmiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Select Product Item</h4>
            </div>
            <div class="modal-body table-responsive">
                <table class="table table-bordered table-striped" id="table1">
                    <thead>
                        <tr>
                            <th>Barcode</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Unit</th>
                            <th>Stock</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($item as $key => $data) {?>
                        <tr>
                                <td><?=$data->barcode?></td>
                                <td><?=$data->name?></td>
                                <td><?=$data->category_name?></td>
                                <td><?=$data->unit_name?></td>
                                <td><?=$data->stock?></td>
                                <td>
                                    <button class="btn btn-xs btn-info" id="select"
                                    data-id="<?=$data->item_id?>"
                                    data-barcode="<?=$data->barcode?>"
                                    data-name="<?=$data->name?>"
                                    data-category="<?=$data->category_name?>"
                                    data-unit="<?=$data->unit_name?>"
                                    data-stock="<?=$data->stock?>">
                                        <i class="fa fa-check"></i> Select
                                    </button>
                                </td>
                            <?php }?>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(document).on('click','#select',function () {
            var item_id = $(this).data('id');
            var barcode = $(this).data('barcode');
            var name = $(this).data('name');
            var category_name = $(this).data('category');
            var unit_name = $(this).data('unit');
            var stock = $(this).data('stock');
            $('#item_id').val(item_id);
            $('#barcode').val(barcode);
            $('#item_name').val(name);
            $('#category_name').val(category_name);
            $('#unit_name').val(unit_name);
            $('#stock').val(stock);
            $('#modal-item').modal('hide');
        })
        
    })
</script>