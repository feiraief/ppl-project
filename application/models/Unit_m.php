<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class unit_m extends CI_Model {

    //menampilkan data unit sesuai ID
    public function get($id = null){

        $this->db->select('*');
        $this->db->from('p_unit');

        if ($id != null) {

            $this->db->where('unit_id', $id);
        }
        
        $query = $this->db->get();
        return $query;

    }
    //delete data user
    public function del($id){
		$this->db->where('unit_id',$id);
		$this->db->delete('p_unit');

	}

    public function add($post){
        $params =[
            'name' => $post['unit_name'],
            
        ];
        $this->db->insert('p_unit',$params);
    }

    public function edit($post){
        $params =[
            'name' => $post['unit_name'],
            
            'updated' => date('Y-m-d h:m:s'),
        ];
        $this->db->where('unit_id',$post['id']);
        $this->db->update('p_unit',$params);
    }

    
}
