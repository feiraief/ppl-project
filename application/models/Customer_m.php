<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_m extends CI_Model {

    //menampilkan data customer sesuai ID
    public function get($id = null){

        $this->db->select('*');
        $this->db->from('customer');

        if ($id != null) {

            $this->db->where('customer_id', $id);
        }
        
        $query = $this->db->get();
        return $query;

    }
    //delete data user
    public function del($id){
		$this->db->where('customer_id',$id);
		$this->db->delete('customer');

	}

    public function add($post){
        $params =[
            'name' => $post['customer_name'],
            'gender' => $post['gender'],
            'phone' => $post['phone'],
            'address' => $post['addr'],
            
        ];
        $this->db->insert('customer',$params);
    }

    public function edit($post){
        $params =[
            'name' => $post['customer_name'],
            'gender' => $post['gender'],
            'phone' => $post['phone'],
            'address' => $post['addr'],
            'updated' => date('Y-m-d h:m:s'),
        ];
        $this->db->where('customer_id',$post['id']);
        $this->db->update('customer',$params);
    }

    
}
