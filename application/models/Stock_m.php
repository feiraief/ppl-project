<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_m extends CI_Model {

   public function get($id = null){
        $this->db->from('t_stock');
        if ($id != null) {
            $this->db->where('stock_id',$id);
        }
        $query = $this->db->get();
        return $query;
   }

    //delete data 
    public function del($id){
		$this->db->where('stock_id',$id);
		$this->db->delete('t_stock');

	}
    // STOCK IN
    public function get_stock_in(){
        $this->db->select('t_stock.stock_id, p_item.item_id, p_item.barcode, 
        p_item.name as item_name, p_category.name as category_name, p_unit.name as unit_name,
        qty, date ,detail , t_stock.created as tanggal, supplier.name as supplier_name');
        $this->db->from('t_stock');
        $this->db->join('p_item', 't_stock.item_id = p_item.item_id');
        $this->db->join('supplier', 't_stock.supplier_id = supplier.supplier_id','left');
        $this->db->join('p_category', 'p_item.category_id = p_category.category_id');
        $this->db->join('p_unit', 'p_item.unit_id = p_unit.unit_id');
        $this->db->where('type', 'in');
        $this->db->order_by('stock_id','desc');
        $query = $this->db->get();
        return $query;
    }

    public function add_stock_in($post){
        $params =[
            'item_id' => $post['item_id'],
            'type' => 'in',
            'detail' => $post['detail'] == '' ? null : $post['detail'],
            'supplier_id' => $post['supplier'] == '' ? null : $post['supplier'],
            'qty' => $post['qty'],
            'date' => $post['date'],
            'user_id' => $this->session->userdata('userid'),
            
        ];
        $this->db->insert('t_stock',$params);
    }

    // STOCK OUT
    public function get_stock_out(){
        $this->db->select('t_stock.stock_id, p_item.item_id, p_item.barcode, 
        p_item.name as item_name, p_category.name as category_name, p_unit.name as unit_name,
        qty, date ,detail , t_stock.created as tanggal, supplier.name as supplier_name');
        $this->db->from('t_stock');
        $this->db->join('p_item', 't_stock.item_id = p_item.item_id');
        $this->db->join('supplier', 't_stock.supplier_id = supplier.supplier_id','left');
        $this->db->join('p_category', 'p_item.category_id = p_category.category_id');
        $this->db->join('p_unit', 'p_item.unit_id = p_unit.unit_id');
        $this->db->where('type', 'out');
        $this->db->order_by('stock_id','desc');
        $query = $this->db->get();
        return $query;
    }

    public function add_stock_out($post){
        $params =[
            'item_id' => $post['item_id'],
            'type' => 'out',
            'detail' => $post['detail'] == '' ? null : $post['detail'],
            'supplier_id' => $post['supplier'] == '' ? null : $post['supplier'],
            'qty' => $post['qty'],
            'date' => $post['date'],
            'user_id' => $this->session->userdata('userid'),
            
        ];
        $this->db->insert('t_stock',$params);
    }

   

    
}
