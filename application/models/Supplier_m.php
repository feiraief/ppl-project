<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Supplier_m extends CI_Model {

    //menampilkan data supplier sesuai ID
    public function get($id = null){

        $this->db->select('*');
        $this->db->from('supplier');

        if ($id != null) {

            $this->db->where('supplier_id', $id);
        }
        
        $query = $this->db->get();
        return $query;

    }
    //delete data user
    public function del($id){
		$this->db->where('supplier_id',$id);
		$this->db->delete('supplier');

	}

    public function add($post){
        $params =[
            'name' => $post['supplier_name'],
            'phone' => $post['phone'],
            'address' => $post['addr'],
            'description' => empty($post['desc']) ? null : $post['desc'],
        ];
        $this->db->insert('supplier',$params);
    }

    public function edit($post){
        $params =[
            'name' => $post['supplier_name'],
            'phone' => $post['phone'],
            'address' => $post['addr'],
            'description' => empty($post['desc']) ? null : $post['desc'],
            'updated' => date('Y-m-d h:m:s'),
        ];
        $this->db->where('supplier_id',$post['id']);
        $this->db->update('supplier',$params);
    }

    
}
