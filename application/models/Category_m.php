<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class category_m extends CI_Model {

    //menampilkan data category sesuai ID
    public function get($id = null){

        $this->db->select('*');
        $this->db->from('p_category');

        if ($id != null) {

            $this->db->where('category_id', $id);
        }
        
        $query = $this->db->get();
        return $query;

    }
    //delete data user
    public function del($id){
		$this->db->where('category_id',$id);
		$this->db->delete('p_category');

	}

    public function add($post){
        $params =[
            'name' => $post['category_name'],
            
        ];
        $this->db->insert('p_category',$params);
    }

    public function edit($post){
        $params =[
            'name' => $post['category_name'],
            
            'updated' => date('Y-m-d h:m:s'),
        ];
        $this->db->where('category_id',$post['id']);
        $this->db->update('p_category',$params);
    }

    
}
