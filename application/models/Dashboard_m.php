<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_m extends CI_Model {

    //menampilkan data supplier sesuai ID
    public function get(){

        $this->db->select('*');
        $this->db->from('p_item');
        $this->db->where('stock > 0 AND stock <20');
        
        
        $query = $this->db->get();
        return $query;

    }
   
   public function gethabis(){

        $this->db->select('*');
        $this->db->from('p_item');
        $this->db->where('stock = 0 ');
        
        $query = $this->db->get();
        return $query;

    } 
    
}
