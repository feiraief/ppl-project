<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct(){
		parent :: __construct();
		check_not_login();
		//cek admin atau bukan
		check_admin();

		$this->load->model('user_m');
		$this->load->library('form_validation');
	}

	public function index()
	{

        $data['row'] = $this->user_m->get();

		$this->template->load('template', 'user/user_data', $data);
	}

	//create data user
	public function add(){

		$this->form_validation->set_rules('fullname', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|is_unique[user.username]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
		$this->form_validation->set_rules('passconf', 'Password confirmation', 'required|matches[password]',
			array('matches' => '%s konfirmasi PASSWORD TIDAK sama')
		);
		$this->form_validation->set_rules('level', 'level', 'required');

		$this->form_validation->set_error_delimiters('<small class="help-block">','</small>');
		//cek validasi
		if ($this->form_validation->run() == FALSE) {
			$this->template->load('template', 'user/user_form_add');
		}
		//valid == tambah
		else {
			$post = $this->input->post(null, TRUE);
			$this->user_m->add($post);
			if ($this->db->affected_rows() > 0) {
				
				echo "<script>alert('selamat, data berhasil ditambahkan');</script>";
				
			}
			echo"<script>window.location = '".site_url('user')."'</script>";
		}


	}

	//edit data user
	public function edit($id){

		$this->form_validation->set_rules('fullname', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[5]|callback_username_check');
		
		if($this->input->post('password')){
			$this->form_validation->set_rules('password', 'Password', 'min_length[5]');
			$this->form_validation->set_rules('passconf', 'Password confirmation', 'matches[password]',
				array('matches' => '%s konfirmasi PASSWORD TIDAK sama')
			);
		}
		if($this->input->post('passconf')){
			$this->form_validation->set_rules('passconf', 'Password confirmation', 'matches[password]',
				array('matches' => '%s konfirmasi PASSWORD TIDAK sama')
			);
		}
		$this->form_validation->set_rules('level', 'level', 'required');

		$this->form_validation->set_error_delimiters('<small class="help-block">','</small>');
		//cek validasi
		if ($this->form_validation->run() == FALSE) {
			$query = $this->user_m->get($id);

			if($query->num_rows() > 0){

				$data['row'] = $query->row();
				$this->template->load('template', 'user/user_form_edit', $data);

			}else {
				echo "<script>alert('DATA tidak ditemukan');</script>";
				echo"<script>window.location = '".site_url('user')."'</script>";
			}

			
		}
		//valid == edit
		else {
			$post = $this->input->post(null, TRUE);
			$this->user_m->edit($post);
			if ($this->db->affected_rows() > 0) {
				
				echo "<script>alert('data berhasil dirubah');</script>";
				
			}
			echo"<script>window.location = '".site_url('user')."'</script>";
		}
	}
	function username_check(){
		$post = $this->input->post(null, TRUE);
		$query = $this->db->query("SELECT * FROM user WHERE username = '$post[username]' AND user_id != '$post[user_id]'");
		if ($query->num_rows() > 0) {
			$this->form_validation->set_message('username_check','{field} ini sudah dipakai');
			return FALSE;
		}else{
			return TRUE;
		}
	}


	//delete data user
	public function del(){
		$id = $this->input->post('user_id');
		$this->user_m->del($id);
		if ($this->db->affected_rows() > 0) {
				
			echo "<script>alert('data berhasil dihapus');</script>";
			
		}
		echo"<script>window.location = '".site_url('user')."'</script>";
	}

}
